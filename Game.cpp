/* 
 Daniel Mallon
 dmallon1@jhu.edu
 Tony Jiang
 tjiang11@jhu.edu
 Devin Sun
 dsun15@jhu.edu
 Lawrence Wolf-Sonkin
 lwolfso1@jhu.edu
 
 Game will be the main class for the entire game.  It will have the two players, and
 the pile of the cards and tokens, as well as some pointer to whose turn it actually
 is.
 */

#include <cstdio>
#include <algorithm>
#include <iostream>
#include "Game.h"
#include "Card.h"
#include "Human.h"
#include "Computer.h"
#include "Token.h"

using std::cout; using std::endl;
using std::cin; using std::string;


Game::Game()
{
    this->itp = players.begin();
}

Game::~Game()
{
    reset();
}


void Game::setUpGame() {
    string name1, name2;
    int hOrC1, hOrC2;
    cout << "Enter the name of player 1:\n";
    //cin >> name1;
    getline(cin,name1);
    //cin.ignore();

    cout << "Enter whether player 1 is human(0) or computer(1):\n";
    cin >> hOrC1;
    while(cin.fail() || !(hOrC1 == 0 || hOrC1==1))
      {
        cin.clear();
        cin.ignore(std::numeric_limits<std::streamsize>::max(),'\n');
        cout << "Did I ask for anything other than 0 or 1? Try again please:"<<endl;
        cin >> hOrC1;
      }    
    /* while (hOrC1 != 0 && hOrC1 != 1) {
        std::cout << "Invalid choice.  Enter whether player 1 is human(0) or computer(1):\n";
        std::cin >> hOrC1;
	}*/
    cout << "Enter the name of player 2:\n";
    cin.ignore();
    getline(cin,name2);
    //cin.ignore();
    //cin >> name2;
    cout << "Enter whether player 2 is human(0) or computer(1):\n";
    cin >> hOrC2;
    while(cin.fail() || !(hOrC2==0 || hOrC2==1))
      {
        cin.clear();
        cin.ignore(std::numeric_limits<std::streamsize>::max(),'\n');
        cout << "Did I ask for anything other than 0 or 1? Try again please:"<<endl;
        cin >> hOrC2;
      }
    /*while (hOrC2 != 0 && hOrC2 != 1) {
        std::cout << "Invalid choice.  Enter whether player 2 is human(0) or computer(1):\n";
        std::cin >> hOrC2;
	}*/
    
    if (hOrC1 == 0)
        addPlayer(new Human(name1));
    else if (hOrC1 == 1)
        addPlayer(new Computer(name1));

    if (hOrC2 == 0)
        addPlayer(new Human(name2));
    else if (hOrC2 == 1)
        addPlayer(new Computer(name2));

    this->itp = players.begin();

    //players created in the game
}

void Game::setupRound()
{
    reset();
    initDeck();
    auto itm = market.end();
    for (int i = 0; i < 3; ++i)
        {
            itm = market.end();
            market.insert(itm, deck.back());
            deck.pop_back();
        }    
    srand(static_cast<unsigned int>(time(NULL))); // removes warning, means same thing
    
    std::random_shuffle(deck.begin(), deck.end(), myrandom);
    //commented out shuffling for testing
    for (int i = 0; i < 5; i++)
    {
        for (int j = 0; j < NUMPLAYERS; j++)
        {
            (*(loop()))->addCard(deck.front());
            deck.erase(deck.begin());
        }
    }
    
    fillMarket();
    
    for (int i = 0; i < NUMPLAYERS; i++)
        (*(loop()))->camelsToHerd();

    initTokens();
}

void Game::reset()
{
    this->market.clear();
    this->deck.clear();


    // clearing token piles
    Ttype Ttypera[] = allTtypes; //typedef'd Ttype array defined in Token.h
    int howmany = sizeof(Ttypera) / sizeof(Ttype);
    for (int i = 0; i < howmany; ++i)
    {
        for (std::vector<Token*>::iterator n = this->tokenPiles[Ttypera[i]].begin(); n != this->tokenPiles[Ttypera[i]].end(); ++n)
        {
            delete *n; //clears each token in the pile
        }
        this->tokenPiles[Ttypera[i]].clear(); //clears each vector token pile
    }
    this->tokenPiles.clear(); //clears the map

    //player-owned things to clear
    for (int i = 0; i < NUMPLAYERS; ++i) //for each player
    {
        (*this->itp)->reset(); //reset player things
        loop(); //switch players
    }

}

void Game::initDeck() {
    
    Card cardD(Card::DIAMOND);
    Card cardG(Card::GOLD);
    Card cardS(Card::SILVER);
    Card cardC(Card::CLOTH);
    Card cardSp(Card::SPICE);
    Card cardL(Card::LEATHER);
    Card cardCa(Card::CAMEL);
    
    std::vector<Card>::iterator it = deck.begin();
    
    deck.insert(it, 6, cardD);
    it = deck.end();
    deck.insert(it, 6, cardG);
    it = deck.end();
    deck.insert(it, 6, cardS);
    it = deck.end();
    deck.insert(it, 8, cardC);
    it = deck.end();
    deck.insert(it, 8, cardSp);
    it = deck.end();
    deck.insert(it, 10, cardL);
    it = deck.end();
    deck.insert(it, 11, cardCa);
    
}

void Game::makeTPile(Ttype t, int valra[], int size)
{
    for (int i = 0; i < size; ++i)
        this->tokenPiles[t].push_back(new Token(t, valra[i] ));
        
        /*if (t == BONUS3 || t == BONUS4 || t == BONUS5)
        {
            this->tokenPiles[t].push_back(new Token(t, valra[i] ));
        }
        else if (t == CAMEL)
        {
            this->tokenPiles[t].push_back(new class Camel());
        }
        else
        {
            this->tokenPiles[t].push_back(new Token(t, valra[i] ));
        }*/
}

void Game::initTokens()
{
    int dra[] = {5,5,5,7,7};
    makeTPile(DIAMOND,dra,5);
    
    int gra[] = {5,5,5,6,6};
    makeTPile(GOLD,gra,5);

    int sira[] = {5,5,5,5,5};
    makeTPile(SILVER,sira,5);
    
    int spra[] = {1,1,2,2,3,3,5};
    makeTPile(SPICE,spra,7);

    int clra[] = {1,1,2,2,3,3,3};
    makeTPile(CLOTH,clra,7);

    int lera[] = {1,1,1,1,1,1,2,3,4};
    makeTPile(LEATHER,lera,9);
    
    int cra[] = {5};
    makeTPile(CAMEL,cra,1);
    
    //bonuses begin

    int b3ra[] = {1,1,2,2,2,3,3};
    makeTPile(BONUS3,b3ra,7);
    
    int b4ra[] = {4,4,5,5,6,6};
    makeTPile(BONUS4,b4ra,6);
    
    int b5ra[] = {8,8,9,10,10};
    makeTPile(BONUS5,b5ra,5);
    
}

bool Game::fillMarket()
{
    bool emptydeck = false;
    int n = 0;
    
    while(market.size() < 5 && emptydeck == false) {
        market.push_back(deck.front());
        deck.erase(deck.begin());
        emptydeck = deck.empty();
        n++;
    }
    
    return emptydeck; //return true if empty deck
}

void Game::playGame() {
    
    setUpGame();
    while (gameEnd() == false)
        playRound();
    reset(); //deletes all allocated memory
}

void Game::playRound() {
    setupRound();
    while (roundEnd() == false)
    {
        (*(loop()))->move(this);
    }

    //end of round behavior i.e. giving out the camel token & seal of excellence
    //and choosing the next starting player

    auto plit1 = players.begin();
    auto plit2 = plit1 + 1;
    this->itp = plit1;

    if ((*plit1)->getHerd() == (*plit2)->getHerd())
    {
        
    }
    else if ((*plit1)->getHerd() > (*plit2)->getHerd())
    {
        (*plit1)->addGoodsToken(this, Card(Card::CAMEL) );
    }
    else
    {
        (*plit2)->addGoodsToken(this, Card(Card::CAMEL) );
    }

    if (*(*plit1) > *(*plit2)) //player 1 is richer
    {

    }
    else //player 2 is richer
    {
        loop();
    }
    cout << (*(this->itp))->getName() << " won the round" << endl;
    (*(this->itp))->awardExcellence();
    loop(); //this is logic to set each player to the current player after excellence awarding
    //basically, the loser goes first next time
}


bool Game::gameEnd() {
        
    for (int i = 0; i < NUMPLAYERS; ++i)
    {
        if ((*(this->itp))->getExcellence() == 2)
        {
            cout << (*(this->itp))->getName() << " won the game." << endl;
            return true;
        }
        loop(); //loop to the next one if not the winner (also will go back to current player if no one won)
    }
    return false;
}

bool Game::roundEnd()
{    
    if (deck.empty())
        return true;
    
    //also check if 3 or more token stacks are empty
    int emptycount = 0;

    Ttype types[] = allTtypes; //we are only supposed to check the goods piles
    int howmany = sizeof(types)/sizeof(Ttype);

    for (int i = 0; i < howmany; ++i)
        if (tokenPiles[types[i]].empty())
            emptycount++;
    
    if (emptycount >= 3)
        return true;

    return false;   
}

void Game::addPlayer(Player * p)
{
    this->players.push_back(p);
    this->itp = players.begin();
}

void Game::displayMarket()
{
    cout << "---MARKET---" << endl;
    int i = 0;
    for(std::vector<Card>::const_iterator it = market.begin(); it!=market.end();it++)
    {
      i++;
      cout << i << ". " << it->getTypeString() << "  ";
    }
    cout << endl;
}

void Game::displayTokens()
{
  cout << "---TOKENS REMAINING---" << endl;
  cout << "Diamond: " << tokenPiles[DIAMOND].size();
  cout << " Gold: " << tokenPiles[GOLD].size();
  cout << " Silver: " << tokenPiles[SILVER].size();
  cout << " Cloth: " << tokenPiles[CLOTH].size();
  cout << " Spice: " << tokenPiles[SPICE].size();
  cout << " Leather: " << tokenPiles[LEATHER].size();
  cout << " Camel: " << tokenPiles[CAMEL].size();
  cout << " Bonus: " << tokenPiles[BONUS3].size() +
    tokenPiles[BONUS4].size() + tokenPiles[BONUS5].size() << endl;
}
std::vector<Player*>::iterator Game::loop()
{
    if ((this->itp + 1) == this->players.end())
    {
        auto old = this->itp;
        this->itp = this->players.begin();
        return old;
    }
    else
     {
        return this->itp++;
    }
}

int Game::countGoodsInMarket() {
    // string goods[] = {"DIAMOND", "GOLD", "SILVER", "CLOTH", "SPICE", "LEATHER"};
    int count = 0;
    for(std::vector<Card>::const_iterator it = market.begin(); it!=market.end();it++)
    {
        if (it->getType() != Card::CAMEL)
            count++;
    }
    return count;
}

bool Game::isGoodInMarket(int spot) {
    
    if (spot >= 1 && spot <= static_cast<int>(market.size()) && this->market[spot - 1].getType() != Card::CAMEL)
    {
        return true;
        // }
    }
    else
        return false;
    
}

int Game::numCamelsInMarket() {
    int count = 0;
    for(std::vector<Card>::const_iterator it = market.begin(); it!=market.end();it++)
    {
        if (it->getType() == Card::CAMEL)
            count++;
    }
    return count;
}


