/* 
 Daniel Mallon
 dmallon1@jhu.edu
 Tony Jiang 
 tjiang11@jhu.edu
 Devin Sun
 dsun15@jhu.edu
 Lawrence Wolf-Sonkin
 lwolfso1@jhu.edu
 
 Token is an abstract class that stores the value of the
 given token, and returns the type via the getType function
 that is virtual and calls the getType function in it's
 children classes */

#include <string>
#include <iostream>
#include "Token.h"

Token::Token()
{
    val = 0;
}
Token::Token(Ttype type, int val)
{
    this->val = val;
    this->type = type;
}
Token::~Token()
{
  
}

std::string Token::getTypeString() const
{
    std::string typestring = "";
    switch (getType())
    {
        case DIAMOND: typestring = "DIAMOND"; break;
        case GOLD: typestring = "GOLD"; break;
        case SILVER: typestring = "SILVER"; break;
        case CLOTH: typestring = "CLOTH"; break;
        case SPICE: typestring = "SPICE"; break;
        case LEATHER: typestring = "LEATHER"; break;
        case CAMEL: typestring = "CAMEL"; break;
        case BONUS3: typestring = "BONUS3"; break;
        case BONUS4: typestring = "BONUS4"; break;
        case BONUS5: typestring = "BONUS5"; break;
    }
    return typestring;
}
