/*
Daniel Mallon
dmallon1@jhu.edu
Tony Jiang
tjiang11@jhu.edu
Devin Sun
dsun15@jhu.edu
Lawrence Wolf-Sonkin
lwolfso1@jhu.edu
*/
#include <iostream>
#include "Game.h"
#include "Card.h"

int main() {
    std::cout << "Welcome to Jaipur!\n";
        
    Game game;
    
    game.playGame();
    
    return 0;
}
