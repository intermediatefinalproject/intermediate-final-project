/* 
 Daniel Mallon
 dmallon1@jhu.edu
 Tony Jiang
 tjiang11@jhu.edu
 Devin Sun
 dsun15@jhu.edu
 Lawrence Wolf-Sonkin
 lwolfso1@jhu.edu
 
 Card class makes up the individual cards for the game.  These cards are
 put into the deck and then dealt out to the players for the game.
 */

#include <stdio.h>
#include "Card.h"
#include <string>


std::string Card::getTypeString() const
{
    std::string typestring = "";
    switch (getType())
    {
        case DIAMOND: typestring = "DIAMOND"; break;
        case GOLD: typestring = "GOLD"; break;
        case SILVER: typestring = "SILVER"; break;
        case CLOTH: typestring = "CLOTH"; break;
        case SPICE: typestring = "SPICE"; break;
        case LEATHER: typestring = "LEATHER"; break;
        case CAMEL: typestring = "CAMEL";
    }
    return typestring;
}


Card::Card(CType type): cardType(type)
{
    
}
