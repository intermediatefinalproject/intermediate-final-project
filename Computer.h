/*  
 Daniel Mallon
 dmallon1@jhu.edu
 Tony Jiang
 tjiang11@jhu.edu
 Devin Sun
 dsun15@jhu.edu
 Lawrence Wolf-Sonkin
 lwolfso1@jhu.edu
 
 Computer class is a player and it is simply the same functions of player
 with an overwritted move function that will use a random number generator to pick the move for the
 computer. 
 */

#ifndef _COMPUTER_H
#define _COMPUTER_H

#include "Player.h"
#include "Token.h"
#include <string>

using std::string;

class Computer: public Player {
public:
  Computer(string pname = "");
  int selectMove(Game* game); /* 0 or 1 */
  int select_takeCards(Game* game);
  int askHowManyGoods(Game* game);
  int askWhichGood(Game* game);
  int askGoodToMarket();
  //  int askWhichGood_again();
  Card askDeleteWhichCard();
  Card askDeleteWhichCard_again();
  int howManyToDelete(Card c);
  /****AI FUNCTIONS****/
  bool laterHalfOfGame(Game* game);
  Card::CType goForGem(Game* game);
  int howManyGems(Game* game);
};

#endif
