/* 
 Daniel Mallon
 dmallon1@jhu.edu
 Tony Jiang
 tjiang11@jhu.edu
 Devin Sun
 dsun15@jhu.edu
 Lawrence Wolf-Sonkin
 lwolfso1@jhu.edu
 
 Player is an abstract that will extend both the human and the computer class
 and will have all the info on the players and the cards and tokens.
 */

#include <iostream>
#include <vector>
#include <string>
#include "Player.h"
#include "Card.h"
#include "Token.h"
#include "Game.h"


using std::cout;
using std::endl;
using std::cin;
using std::cerr;
Player::Player(std::string pname) : name(pname), excellence(0), hand(), tokens(), herd()
{

}

void Player::reset()
{
  this->herd.clear(); //clear herd
  this->hand.clear(); //clear hand
  for (std::vector<Token*>::iterator i = this->tokens.begin(); i != this->tokens.end(); ++i)
  {
      delete *i; //clear each token
  }
  this->tokens.clear(); //clear tokens array
}

void Player::move(Game* game) {
  cout << "~~~~~~~~~~~~~~~~~~~~"; cout << getName() << "'s turn~~~~~~~~~~~~~~~~~~~~" << endl;
    game->displayMarket();
    game->displayTokens();
    cout << "Cards in deck remaining: " << game->deck.size() << endl;
    cout << getName() << "'s WEALTH: " << getWealth() << " rupees" <<  endl;
    cout << getName() << "'s HERD: " << getHerd() << " camel(s)" << endl;
    int option;
    bool valid = false;
    do {
        valid = true;
        try {
            option = selectMove(game); //For computer selectMove will be random. For Human selectMove will be user input.
           
        }
        catch(std::invalid_argument & e) {
      cerr<< e.what()<<endl;
      valid =false;
      //char ch;
      //while((ch=getchar()) != '\n' && ch != EOF);
      //cin.clear();
      //cin.ignore(std::numeric_limits<std::streamsize>::max(),'\n');
      //cin.ignore();
        }
    }  while (!valid);
  if(option == 0) //SellCards
    {
      sellCards(game);
      cout << "New WEALTH: " << getWealth() << " rupees" << endl;
    }
  else if(option == 1)
    {
      takeCards(game);
    } 
}

void Player::sellCards(Game* game) {
    Card delcard = askDeleteWhichCard();
    int n = howManyToDelete(delcard);
    std::vector<Card>::iterator it = hand.begin();
    while (n < 2 && (delcard.getTypeString() == "GOLD" || delcard.getTypeString() == "SILVER" || delcard.getTypeString() == "DIAMOND")) {
        delcard = askDeleteWhichCard_again();
        n = howManyToDelete(delcard.getType());
        it = hand.begin();
    }
        
    if (n >= 3 && n <= 5)
        addBonusToken(game, n);        
    while (n > 0)
    {
        if (it->getType()==delcard.getType())
        {
            addGoodsToken(game, *it);
            hand.erase(it);
            n--;
        }
        else
            it++;
    }
}

void Player::takeCards(Game* game) {
    int option = select_takeCards(game); //For computer select_takeCards() will be random. For Human will be user input.
    if (option == 0) /** TAKE SEVERAL GOODS - take as many good cards as you want from the market and exchange the same number of cards (goods or camels)*/
    {
        int howManyGoods, whichGood, count = 0;
        howManyGoods = askHowManyGoods(game);
        while(count < howManyGoods)
        {
            if (count != 0)
            {
                game->displayMarket();
            }
            whichGood = askWhichGood(game);
            while(game->market.at(whichGood).getType() == Card::CAMEL)
            {
                whichGood = askWhichGood(game);
            }
            marketToHand(whichGood, game);
            count++;
        }
        //put cards back
        howManyGoods = 0;
        while(count > howManyGoods)
        {
            whichGood = askGoodToMarket();

            if (whichGood == -1)
                camelToMarket(game);
            else
                handToMarket(whichGood, game);
            howManyGoods++;
        }
    }
    if (option == 1) /** TAKE ONE SINGLE GOOD - take a single good from the market and replace the card with a card from the top of deck */
    {
        int whichGood = askWhichGood(game);
        while(game->market.at(whichGood).getType() == Card::CAMEL) {
        whichGood = askWhichGood(game);
    }
      marketToHand(whichGood, game);    
      game->fillMarket();
    }
    if (option == 2) /** TAKE THE CAMELS - take all camels from market and add to herd then replace cards in market by drawing from deck*/
    {
        for(std::vector<Card>::iterator itr = game->market.begin(); itr != game->market.end(); itr++)
        {
        if(itr->getType() == Card::CAMEL)
            {
              hand.push_back(*itr);
              game->market.erase(itr);
              itr--;
            }
        } 
        game->fillMarket();
        camelsToHerd();
	cout << "New HERD: " << getHerd() << " camel(s)" << endl;
    }
}
void Player::marketToHand(int choice, Game* game)
{
    if (choice >= 0 && choice <= static_cast<int> (game->market.size()) - 1)
    {
        hand.push_back(game->market[choice]);
        game->market.erase(game->market.begin() + choice);
    }
}

void Player::handToMarket(int choice, Game* game) {    
    game->market.push_back(hand[choice]);
    hand.erase(hand.begin() + choice);
}

void Player::camelToMarket(Game* game) {
    game->market.push_back(Card::CAMEL);
    herd.erase(herd.begin());
}

void Player::dispHand() const {  
    cout << "---" << this->name << "\'s HAND---" << endl;
    int i=0;
    for(std::vector<Card>::const_iterator it = hand.begin(); it!=hand.end();it++)
    {
       i++;
       cout << i << ". " << (*it).getTypeString() << "  ";
    }
    cout << endl;
}

void Player::dispTokens() const {

}

int Player::getWealth() const {
    int wealth = 0;
    for (std::vector<Token *>::const_iterator it = tokens.begin(); it!=tokens.end();it++)
    {
      wealth+=(*it)->getVal();
    }
    //i dont remember if there are any other sources of wealth
    return wealth;
}

void Player::addCard(Card c) {

    std::vector<Card>::iterator ith = hand.begin();
    hand.insert(ith, c);
    
}

bool Player::addBonusToken(Game* game, int n)
{
    if (n >= 3 && n <= 5)
    {
        Ttype t =  static_cast <Ttype> (BONUS3 - 3 + n);
        
        std::vector<Token*>::iterator it = game->tokenPiles[t].end();

        if (it != game->tokenPiles[t].begin())
        {
            it--;
            tokens.push_back(*it);
            // game->tokenPiles[getTokenType(cardtype)].pop_back();
            game->tokenPiles[t].erase(it);
            return true; //true if a token were taken
        }
        else
        {
            return false; //false if no tokens we're taken
        }
    }
    return false;
}

bool Player::addGoodsToken(Game* game, Card c) {
    string cardtype = c.getTypeString();
    
    std::vector<Token*>::iterator it = game->tokenPiles[getTokenType(cardtype)].end();

    if (it != game->tokenPiles[getTokenType(cardtype)].begin() )
    {
        it--;
        tokens.push_back(*it);
        // game->tokenPiles[getTokenType(cardtype)].pop_back();
        game->tokenPiles[getTokenType(cardtype)].erase(it);
        return true; //true if a token were taken
    }
    else
    {
        return false; //false if no tokens we're taken
    }
}

void Player::camelsToHerd() {
    std::vector<Card>::iterator itr = hand.begin();
    while (itr != hand.end()) {
        if (itr->getType() == Card::CAMEL) {
            herd.push_back(*itr);
            hand.erase(itr);
        } 
    else
            itr++;
    }
}

void Player::awardExcellence()
{
  this->excellence++;
}

Ttype Player::getTokenType(string cardtype) {    
    if (cardtype == "DIAMOND")
        return DIAMOND;
    if (cardtype == "GOLD")
        return GOLD;
    if (cardtype == "SILVER")
        return SILVER;
    if (cardtype == "CLOTH")
        return CLOTH;
    if (cardtype == "SPICE")
        return SPICE;
    return LEATHER;
}

int Player::howManyGoodTokens() const
{
    int goodscount = 0;
    Ttype goodsra[] = allGoodTtypes; //define in Goods.h
    int howmanyrpt = sizeof(goodsra) / sizeof(Ttype);
    for (std::vector<Token*>::const_iterator i = this->tokens.begin(); i != this->tokens.end(); ++i)
    {
        for (int n = 0; n < howmanyrpt; ++n)
        {
            if ((*i)->getType() == goodsra[n])
            {
                goodscount++;
            }
        }
    }
    return goodscount;
}

int Player::howManyBonusTokens() const
{
    int bonuscount = 0;
    Ttype bonusra[] = allBonusTtypes; //define in Bonus.h
    int howmanyrpt = sizeof(bonusra) / sizeof(Ttype);
    for (std::vector<Token*>::const_iterator i = this->tokens.begin(); i != this->tokens.end(); ++i)
    {
        for (int n = 0; n < howmanyrpt; ++n)
        {
            if ((*i)->getType() == bonusra[n])
            {
                bonuscount++;
            }
        }
    }
    return bonuscount;
}

bool Player::operator< (const Player & other) const
{
    if (this->getWealth() < other.getWealth()) //if I'm less wealthy: true
    {
        return true;
    }
    else if (this->getWealth() == other.getWealth()) //if we're equally wealthy:
    {
        if (this->howManyBonusTokens() == other.howManyBonusTokens()) //if we have an equal number of bonus tokens:
        {
            if (this->howManyGoodTokens() < other.howManyGoodTokens()) //if I have less good tokens: true
            {
                return true;
            }
            else //If I >= goods tokens, false
            {
                return false;
            }
        }
        else if (this->howManyBonusTokens() < other.howManyBonusTokens()) //If I have less bonus tokens: true
        {
            return true;
        }
        else //if you have more bonus tokens: false
        {
            return false;
        }
    }
    else //If you are wealthier: false
    {
        return false;
    }
}

bool Player::operator> (const Player & other) const
{
    return other < *this; //code reuse
}

bool Player::operator<= (const Player & other) const
{
    return !(*this > other); //code reuse
}

bool Player::operator>= (const Player & other) const
{
    return !(*this < other); //code reuse
}
