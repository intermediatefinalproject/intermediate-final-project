/*  
 Daniel Mallon
 dmallon1@jhu.edu
 Tony Jiang
 tjiang11@jhu.edu
 Devin Sun
 dsun15@jhu.edu
 Lawrence Wolf-Sonkin
 lwolfso1@jhu.edu
 
 Human class is a player and it is simply the same functions of player
 with an overwritted move function.
*/


#ifndef _HUMAN_H
#define _HUMAN_H

#include "Player.h"
#include <exception>
#include <stdexcept>
class Human: public Player {

public:
	Human(std::string pname = "");
    int selectMove(Game* game) throw(std::invalid_argument);
    int select_takeCards(Game* game);
    int askHowManyGoods(Game* game);
    int askWhichGood(Game* game);
    int askGoodToMarket();
    int askWhichGood_again();
    Card askDeleteWhichCard();
    Card askDeleteWhichCard_again();
    int howManyToDelete(Card c);
    void resetBuffer();
};

#endif
