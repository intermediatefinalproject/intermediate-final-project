/* 
 Daniel Mallon
 dmallon1@jhu.edu
 Tony Jiang
 tjiang11@jhu.edu
 Devin Sun
 dsun15@jhu.edu
 Lawrence Wolf-Sonkin
 lwolfso1@jhu.edu
 
 Card class makes up the individual cards for the game.  These cards are
 put into the deck and then dealt out to the players for the game.
*/


#ifndef _CARD_H
#define _CARD_H

#include <string>

class Card
{

public:
    
    typedef enum cardtype
    {
        DIAMOND,
        GOLD,
        SILVER,
        CLOTH,
        SPICE,
        LEATHER,
        CAMEL
    } CType;
    

    Card(CType name);
    std::string getTypeString() const;
    CType getType() const { return cardType; };

private:
    
    CType cardType;
    
};

#endif
