/* 
 Daniel Mallon
 dmallon1@jhu.edu
 Tony Jiang
 tjiang11@jhu.edu
 Devin Sun
 dsun15@jhu.edu
 Lawrence Wolf-Sonkin
 lwolfso1@jhu.edu
 
 Token is an abstract class that stores the value of the
 given token, and returns the type via the getType function
 that is virtual and calls the getType function in it's
 children classes */

#ifndef _TOKEN_H
#define _TOKEN_H

#include <string>

typedef enum tokenType
    {
        DIAMOND,
        GOLD,
        SILVER,
        CLOTH,
        SPICE,
        LEATHER,
        CAMEL,
        BONUS3,
        BONUS4,
        BONUS5
    } Ttype;

#define allTtypes { DIAMOND, GOLD, SILVER, CLOTH, SPICE, LEATHER, CAMEL, BONUS3, BONUS4, BONUS5}
#define allGoodTtypes { DIAMOND, GOLD, SILVER, CLOTH, SPICE, LEATHER, CAMEL}
#define allBonusTtypes {BONUS3, BONUS4, BONUS5}

class Token
{
public:
    Token();
    Token(Ttype type, int val);
	~Token();
	int getVal() { return val; };
    Ttype getType() const { return type; };
    std::string getTypeString() const;

    
protected:
    int val;
    Ttype type;
};

#endif
