/*  
 Daniel Mallon
 dmallon1@jhu.edu
 Tony Jiang
 tjiang11@jhu.edu
 Devin Sun
 dsun15@jhu.edu
 Lawrence Wolf-Sonkin
 lwolfso1@jhu.edu
 
 Computer class is a player and it is simply the same functions of player
 with an overwritted move function that will use a random number generator to pick the move for the
 computer.
 */

#include <iostream>
#include <string>
#include "Computer.h"
#include "Player.h"
#include "Game.h"
#include <cstdlib>
#include <stdlib.h>
#include <ctime>

#define MARKET_SIZE 5

using std::string; using std::cout;
using std::endl;

Computer::Computer(string pname) : Player(pname) {
    
}

int Computer::selectMove(Game* game) {
  dispHand(); /* debugging purposes only */
    srand(static_cast<unsigned int>(time(NULL)));
    int randvar = random() % 5;
    int randtwo = random() % 2;
    int diamond = 0; int silver = 0; int gold = 0;
    for(std::vector<Card>::const_iterator itr = hand.begin(); itr != hand.end(); itr++)
      {
        switch(itr->getType()) {
        case Card::SILVER:
          silver++;
          break;
        case Card::GOLD:
          gold++;
          break;
        case Card::DIAMOND:
          diamond++;
          break;
        default:
          break;
        }
      }
    if (hand.size() <= 3 && silver <= 1 && gold <= 1 && diamond <= 1)
      return 1;
    if(howManyGems(game) >= 2 && randvar!= 0) /*80% - take cards if 3 or more gems in market.*/
      return 1;
    if (hand.size() < 4 && randvar != 0) /*80% chance of making intelligent decision** take cards if has less than four cards.)*/
      return 1;
    return randtwo;
}

int Computer::select_takeCards(Game* game) {
  bool legalZero = true; 
  bool legalOne = true; 
  bool legalTwo = true;
  srand(static_cast<unsigned int>(time(NULL)));
    if ((unsigned)game->countGoodsInMarket() == 1)
        legalZero = false;
    if ((unsigned)game->countGoodsInMarket() == 0)
      {
	legalZero = false;
	legalOne = false;
	return 2;
      }
    if (hand.size() == 7) {
      legalOne = false;
    }
    if ((unsigned)game->numCamelsInMarket() == 0) {
      legalTwo = false;
    }
  int randthree = random() % 3;
  int randvar = random() % 5;
  if((unsigned)game->numCamelsInMarket() > 1 && randvar != 0)
    {/*80% intelligent decision: take camels if more than one camel in market*/
      if(legalTwo == true) {
	cout << getName() << " takes the camels." << endl;
	return 2;
      }
    }
  if(legalTwo == false && randthree == 2)
    randthree = 1;
  if(legalOne == false && randthree == 1)
    randthree = 0;
  if(legalZero == false && randthree == 0)
    randthree = 2;
  cout << getName();
  switch(randthree) {
  case 0: cout << " takes multiple goods." << endl;
    break;
  case 1: cout << " takes a single good." << endl;
    break;
  case 2: cout << " takes the camels." << endl;
  }
  return randthree;
}

int Computer::askHowManyGoods(Game* game) {
  srand(static_cast<unsigned int>(time(NULL)));
  unsigned int randi = random() % (unsigned)game->countGoodsInMarket();
  if(randi > hand.size())
    randi = hand.size();
  while(randi <= 1)
    randi++;
  cout << getName() << " takes " << randi << " good(s): " << endl;
  return randi;
}

Card Computer::askDeleteWhichCard() {
  srand(static_cast<unsigned int>(time(NULL)));
  unsigned int randi = random() % static_cast<unsigned int>(hand.size());
  int count = 0; Card card = hand.at(randi);
  for(std::vector<Card>::const_iterator it = hand.begin(); it != hand.end(); it++)
    {
      if(it->getType() == card.getType())
	count++;
    }
  while((card.getType() == Card::CType::SILVER ||
	 card.getType() == Card::CType::GOLD ||
	 card.getType() == Card::CType::DIAMOND) &&
	count < 2) 
    {
      count = 0;
      if(randi >= static_cast<unsigned int>(hand.size())) 
	randi = 0;
      card = hand.at(randi);
      for(std::vector<Card>::const_iterator it = hand.begin(); it != hand.end(); it++)
	{
	  if(it->getType() == card.getType())
	    count++;
	}
      randi++;
    }
      return card;
}
Card Computer::askDeleteWhichCard_again() {
  srand(static_cast<unsigned int>(time(NULL)));
  int randi = random() % static_cast<unsigned int>(hand.size());
  return hand.at(randi);
}

int Computer::howManyToDelete(Card c) {
  srand(static_cast<unsigned int>(time(NULL)));
  int count = 0;
  for (std::vector<Card>::const_iterator it = hand.begin(); it != hand.end(); it++) {
    if (it->getType() == c.getType())
      count++;
  }
  int randi = random() % count;
  if (c.getType() == Card::CType::GOLD || c.getType() == Card::CType::SILVER || c.getType() == Card::CType::DIAMOND){
    cout << getName() << " sells " << count << " " << c.getTypeString() << " good(s)." << endl;
    return count;
  } 
  if (randi == 0)
    randi = 1;
  cout << getName() << " sells " << randi << " " << c.getTypeString() <<  " good(s)." << endl;
  return randi;
}

int Computer::askWhichGood(Game* game) {
  srand(static_cast<unsigned int>(time(NULL)));
  unsigned int randi = random() % static_cast<unsigned int>(game->market.size());
  unsigned int randvar = random() % 10;
  while(game->market.at(randi).getType() == Card::CType::CAMEL) {
    randi += 1;
    if(randi == game->market.size())
      randi = 0;
  }
  /** AI BEGIN **/
  Card::CType gemCard;
  if(howManyGems(game) == 1 && randvar < 5) 
    gemCard = goForGem(game);
  if(howManyGems(game) == 2 && randvar < 6) 
    gemCard = goForGem(game);
  if(howManyGems(game) == 3 && randvar < 9)
    gemCard = goForGem(game);
  if(howManyGems(game) >= 4)
    gemCard = goForGem(game);
  int count = 0;
  for(std::vector<Card>::const_iterator itr = game->market.begin(); itr != game->market.end(); itr++)
    {
      if(itr->getType() == gemCard) {
	cout << getName() << " takes " << game->market.at(count).getTypeString() << endl;
	return count;
      }
      count++;
    }
  /** AI END **/
    cout << getName() << " takes " << game->market.at(randi).getTypeString() << endl;
    return randi;
}
/*int Computer::askWhichGood_again() {
  srand(static_cast<unsigned int>(time(NULL)));
  int randi = random() % MARKET_SIZE;
  cout << getName() << " takes " << game->market.at(randi).getTypeString() << endl;
  return randi;
  }*/

int Computer::askGoodToMarket() {
  srand(static_cast<unsigned int>(time(NULL)));
  int randi = random() % static_cast<unsigned int>(hand.size());
  cout << getName() << " returns " << hand.at(randi).getTypeString() << " to the market." << endl;
  return randi;
}

/****AI FUNCTIONS*****/

bool Computer::laterHalfOfGame(Game* game) {
  if(game->deck.size() <= 15)
    return true;
  else
    return false;
  return true;
}

Card::CType Computer::goForGem(Game* game) {
  int diamond = 0; int silver = 0; int gold = 0;
  int max = 0; Card::CType maxtype;
  for(std::vector<Card>::const_iterator itr = game->market.begin(); itr != game->market.end(); itr++)
    {
      switch(itr->getType()) {
      case Card::SILVER:
	silver++;
	break;
      case Card::GOLD:
	gold++;
	break;
      case Card::DIAMOND:
	diamond++;
	break;
      default:
	break;
      }
    }
  if(diamond > max) {
    max = diamond;
    maxtype = Card::DIAMOND; 
  }
  if(silver > max) {
    max = silver;
    maxtype = Card::SILVER;
  }
  if(gold > max) {
    max = gold;
    maxtype = Card::GOLD;
  }
  return maxtype;
}

 int Computer::howManyGems(Game* game) {
   int count = 0;
   for(std::vector<Card>::const_iterator itr = game->market.begin(); itr != game->market.end(); itr++)
     {
       if(itr->getType() == Card::SILVER ||
	  itr->getType() == Card::GOLD ||
	  itr->getType() == Card::DIAMOND)
	 {
	   count++;
	 }
     }
   return count;
 }
	  
