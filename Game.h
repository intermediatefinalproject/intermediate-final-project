/* 
 Daniel Mallon
 dmallon1@jhu.edu
 Tony Jiang
 tjiang11@jhu.edu
 Devin Sun
 dsun15@jhu.edu
 Lawrence Wolf-Sonkin
 lwolfso1@jhu.edu
 
 Game will be the main class for the entire game.  It will have the two players, and
 the pile of the cards and tokens, as well as some pointer to whose turn it actually
 is.
*/

#ifndef _GAME_H
#define _GAME_H


#include <map>
#include <vector>
#include <algorithm>
#include <cstdlib>      // std::rand, std::srand
#include "Player.h"
#include "Token.h"

#define NUMPLAYERS 2
typedef std::vector<Player*> pvec;
typedef pvec::iterator pvecit;

class Game
{
private:
    pvec players;
    pvecit itp;

public:
    Game();
    ~Game();

    std::vector<Card> deck;
    std::vector<Card> market;
    std::map<Ttype, std::vector<Token*> > tokenPiles;
    void makeTPile(Ttype t, int valra[], int size);
    void setUpGame();
    void initDeck();
    void initTokens();
    bool fillMarket();
    void playGame();
    void playRound();
    bool gameEnd();
    bool roundEnd();
    void addPlayer(Player * p);
    void addThisToken(std::string tokenName);
    void setupRound();
    void reset();
    pvecit loop();
    int countGoodsInMarket();
    int numCamelsInMarket();
    bool isGoodInMarket(int spot);
    void displayMarket();
    void displayTokens();
    pvec getPlayers() {return players; };
    int static myrandom (int i) { return rand()%i;};
};


#endif
