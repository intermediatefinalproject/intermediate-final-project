/* 
 Daniel Mallon
 dmallon1@jhu.edu
 Tony Jiang
 tjiang11@jhu.edu
 Devin Sun
 dsun15@jhu.edu
 Lawrence Wolf-Sonkin
 lwolfso1@jhu.edu
 
 Player is an abstract that will extend both the human and the computer class
 and will have all the info on the players and the cards and tokens.
*/


#ifndef _PLAYER_H
#define _PLAYER_H

#include <string>
#include <vector>
#include "Card.h"
#include "Token.h"

using std::string;

class Game;

class Player
{
protected:
    
    string name;
    int excellence;
    std::vector<Card> hand;
    std::vector<Token*> tokens;
    std::vector<Card> herd;

public:
    Player(string pname = "");
    void move(Game* game);
    void takeCards(Game* game);
    void marketToHand(int, Game* game);
    void handToMarket(int, Game* game);
    void camelToMarket(Game* game);
    void sellCards(Game* game);
    virtual Card askDeleteWhichCard() = 0;
    virtual Card askDeleteWhichCard_again() = 0;
    virtual int howManyToDelete(Card c) = 0;
    virtual int selectMove(Game* game) = 0; /** Decide on whether to TAKE CARDS or SELL CARDS
                       Overriding:
                       Random selection for computer, prompt user input for human. 
                       return 0 for sellCards() and 1 for takeCards() */
    virtual int select_takeCards(Game* game) = 0; /** Decide on whether to 0.) TAKE SEVERAL GOODS, 1.) TAKE 1 SINGLE GOOD, or 2.) TAKE THE CAMELS
                        Overriding:
                        Random selection for computer, prompt user input for human.*/
    virtual int askHowManyGoods(Game* game) = 0; /** Under TAKE SEVERAL GOODS, ask how many goods to take */
    virtual int askWhichGood(Game* game) = 0; /** Ask the user which good to take from market.*/
    virtual int askGoodToMarket() = 0;
    //  virtual int askWhichGood_again() = 0;
    void dispHand() const;
    void dispTokens() const;
    int getWealth() const;
    void addCard(Card c);
    bool addGoodsToken(Game* game, Card c);
    bool addBonusToken(Game * game, int n); 
    int getExcellence() const { return excellence; };
    void awardExcellence();
    int getHerd() const {return (int)herd.size(); };
    void camelsToHerd();
    void roundWon() { excellence++; };
    void reset();
    Ttype getTokenType(string cardtype);
    int howManyGoodTokens() const;
    int howManyBonusTokens() const;
    std::vector<Card> getHand() const {return hand;};
    std::vector<Token *> getTokens() const {return tokens;};
    string getName() { return name; };
    

    bool operator< (const Player & other) const;
    // bool operator== (const Player & other) const;
    bool operator> (const Player & other) const;
    bool operator<= (const Player & other) const;
    bool operator>= (const Player & other) const;
    // bool operator!= (const Player & other) const;

};

#endif
