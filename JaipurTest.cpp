/*Daniel Mallon
 dmallon1@jhu.edu
 Tony Jiang 
 tjiang11@jhu.edu
 Devin Sun
 dsun15@jhu.edu
 Lawrence Wolf-Sonkin
 lwolfso1@jhu.edu
*/

#include <cassert>
#include <iostream>
#include <algorithm>
#include "Game.h"
#include "Card.h"
#include "Player.h"
#include "Computer.h"
#include "Human.h"

using std::cout; using std::cin; using std::endl;

int main() {
    Game game;
    game.addPlayer(new Computer("Bob"));
    assert(game.getPlayers().size() == 1); // Game has 1 player
    game.initDeck(); // Create deck
    assert(game.deck.size() == 55); // Deck has 55 cards
    assert(game.deck.at(5).getTypeString() == "DIAMOND");
    assert(game.deck.at(15).getTypeString() == "SILVER");
    assert(game.deck.at(25).getTypeString() == "CLOTH");
    assert(game.deck.at(35).getTypeString() == "LEATHER");
    assert(game.deck.at(45).getTypeString() == "CAMEL");
    assert(game.deck.at(54).getTypeString() == "CAMEL");
    game.fillMarket();
    cout << game.numCamelsInMarket() << endl;
    for(int i = 0; i < 5; i++)
    {
        assert(game.market.at(i).getTypeString() == "DIAMOND");
    }
    assert(game.countGoodsInMarket() == 5); // Market has 5 goods
    
    //tokens
    
    game.initTokens();
    assert(game.tokenPiles.size() ==10); // Deck has 55 cards
    assert(game.tokenPiles[DIAMOND].at(3)->getTypeString() != "SILVER");
    assert(game.tokenPiles[SILVER].at(3)->getTypeString() != "DIAMOND");
    assert(game.tokenPiles[CLOTH].at(3)->getTypeString() == "CLOTH");
    assert(game.tokenPiles[LEATHER].at(3)->getTypeString() != "SPICE");
    assert(game.tokenPiles[CAMEL].at(0)->getTypeString() != "SILVER");
   
    //add cards to players hands
    game.setupRound();
    //cout << game.deck.at(5).getTypeString() << endl;
    assert(game.deck.at(5).getTypeString() == "SILVER");
    game.getPlayers().at(0)->addCard(game.deck.at(5));
    assert(game.getPlayers().at(0)->getHand().at(0).getTypeString() == "SILVER");
    
    (*(game.loop()))->addGoodsToken(&game, game.getPlayers().at(0)->getHand().at(0));
    assert((*(game.loop()))->getTokens().at(0)->getTypeString() == "SILVER");
    assert(game.getPlayers().at(0)->getWealth() == 5);
    //cout << game.getPlayers().at(0)->getWealth() << endl;
    // test removing of all of the things above
    game.reset();
    assert(game.market.size() == 0);
    assert(game.deck.size() == 0);
    assert(game.tokenPiles.size() == 0);
    assert(game.getPlayers().at(0)->getHand().size() == 0);
    //    assert(game.getPlayers().at(1)->getHand().size() == 0);
    
    // make sure we test all of our constructors
    Human humanplayer("testname");
    assert(humanplayer.getName() == "testname");
    assert(humanplayer.getExcellence() == 0);
    assert(humanplayer.getWealth() == 0);
    Computer computerplayer("sam");
    assert(computerplayer.getName() == "sam");
    assert(computerplayer.getExcellence() == 0);
    assert(humanplayer.getWealth() == 0);
    Token good(LEATHER, 5);
    assert(good.getType() == LEATHER);
    assert(good.getVal() == 5);
    // test the opertations of selling cards
    
    std::cout << "Test cases passed\n";
    
    
  
}
