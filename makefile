CC = g++
CFLAGS = -std=c++11 -pedantic -Wall -Wextra -O -g
GPROF = -pg
GCOV = -fprofile-arcs -ftest-coverage

# binaries

JaipurMain: JaipurMain.o Card.o Token.o Game.o Human.o Computer.o Player.o
	$(CC) $(CFLAGS) -o JaipurMain JaipurMain.o Card.o Token.o Game.o Human.o Computer.o Player.o
# dot o

JaipurMain.o: JaipurMain.cpp Game.h Card.h
	$(CC) $(CFLAGS) -c JaipurMain.cpp
Token.o: Token.cpp Token.h
	$(CC) $(CFLAGS) -c Token.cpp
Game.o: Game.cpp Game.h Card.h Human.h Token.h
	$(CC) $(CFLAGS) -c Game.cpp
Card.o: Card.cpp Card.h
	$(CC) $(CFLAGS) -c Card.cpp
Human.o: Human.cpp Human.h Player.h
	$(CC) $(CFLAGS) -c Human.cpp
Computer.o: Computer.cpp Computer.h Player.h Token.h 
	$(CC) $(CFLAGS) -c Computer.cpp 
Player.o: Player.cpp Player.h Card.h Token.h Game.h 
	$(CC) $(CFLAGS) -c Player.cpp
JaipurTest:
	$(CC) $(CFLAGS) -O0 -o JaipurTest JaipurTest.cpp Game.cpp Player.cpp Card.cpp Human.cpp Computer.cpp Token.cpp Bonus.cpp Goods.cpp Camel.cpp

# clean

clean:
	rm *.o JaipurMain JaipurTest
