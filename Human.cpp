/*  
 Daniel Mallon
 dmallon1@jhu.edu
 Tony Jiang
 tjiang11@jhu.edu
 Devin Sun
 dsun15@jhu.edu
 Lawrence Wolf-Sonkin
 lwolfso1@jhu.edu
 
 Human class is a player and it is simply the same functions of player
 with an overwritted move function.
 */

#include "Human.h"
#include "Card.h"
#include "Game.h"
#include <iostream>
#include <exception>
#include <stdexcept>
using std::cout; using std::endl;
using std::cin; using std::string;

Human::Human(std::string pname): Player(pname) {

}

int Human::selectMove(Game* game) throw(std::invalid_argument){
    (void)game;
    dispHand();
    int option;
    std::cout << "Do you wish to sell cards(0), or take cards(1)?\n";
    //std::cin.ignore();
    std::cin >> option;
    if(cin.fail())
    {
      cin.clear();
      cin.ignore(std::numeric_limits<std::streamsize>::max(),'\n');
      throw std::invalid_argument("Don't try the break the program, enter 0 or 1: ");
    }    
    if (option == 1)
        return option;
    if (hand.size() == 0)
        throw std::invalid_argument("Invalid choice, no cards to sell.  Do you wish to sell cards(0), or take cards(1)?\n");
    int diamond = 0; int silver = 0; int gold = 0;
    bool onlyGems = true;
    for(std::vector<Card>::const_iterator itr = hand.begin(); itr != hand.end(); itr++)
      {
	switch(itr->getType()) {
	case Card::SILVER:
	  silver++;
	  break;
	case Card::GOLD:
	  gold++;
	  break;
	case Card::DIAMOND:
	  diamond++;
	  break;
	default:
	  onlyGems = false;
	  break;
	}
      }
    if (hand.size() <= 3 && silver <= 1 && gold <= 1 && diamond <= 1 && onlyGems == true)
        throw std::invalid_argument("Invalid choice, no cards to sell.  Do you wish to sell cards(0), or take cards(1)?\n");
    return option;
}

int Human::select_takeCards(Game* game) {
    (void)game;
    int option;
    cout << "Do you wish to take several goods(0), take a single good(1), or take the camels(2)?\n";
    //std::cin.ignore();
    cin >> option;

    while(cin.fail())
      {
        resetBuffer();
	cin >> option;
      }
    if ((option == 0 && hand.size() > 0) || option == 1 || option == 2) {
	while(hand.size() == 7 && option == 1) {
	  cout << "You cannot have more than seven cards in your hand. Choose another option." << endl;
	  cin >> option;
	}
    while(game->numCamelsInMarket() > 3 && option == 0) {
        cout << "There is only one good in the market you cannot take multiple goods." << endl;
        cin >> option;
    }
	while(game->numCamelsInMarket() == 0 && option == 2) {
	  cout << "You cannot take the camels when there are no camels in the market. Choose another option." << endl;
	  cin >> option;
	}
	while(hand.size() == 0 && option == 1)
	  {
	    cout << "You cannot exchange goods when you have no goods. Choose another option." << endl;
	    cin >> option;
	  }
	return option;
      }
      else  
        while (option != 0 && option != 1 && option != 2) {
	  cout << "Invalid choice.  Do you wish to take several goods(0), take a single good(1), or take the camels(2)?" << endl;
            cin >> option;
        }
    return option;
}

Card Human::askDeleteWhichCard()
{
  //    dispHand();
    cout << "Enter the number corresponding to the card you want to sell. Yo\
u can sell multiple cards of the same type:" << endl;
    unsigned int choice;
    //cin.ignore();
    cin >> choice;
    while(cin.fail())
      {
        resetBuffer();
        cin >> choice;
      }
    while (choice<1 || choice > hand.size())
      {
	cout << "Invalid number, try again:" << endl;
	cin >> choice;
      }
    return hand.at(choice-1);
}

Card Human::askDeleteWhichCard_again()
{
    cout << "You cannot sell less than 2 gold, silver, or diamond cards. Pick another good to sell:" << endl;
    unsigned int choice;
    cin >> choice;
    while(cin.fail())
      {
        resetBuffer();
        cin >> choice;
      }
    while (choice<1 || choice > hand.size())
      {
        cout << "Invalid number, try again:" << endl;
        cin >> choice;
      }
    return hand.at(choice-1);
}

    
int Human::howManyToDelete(Card c)
{    
    int count =0;
    for (std::vector<Card>::const_iterator it = hand.begin(); it!=hand.end(); it++)
    {
      if (it->getType() == c.getType())
      {
	count++;
      }
    }
    cout << "You have " << count << " " << c.getTypeString() << " good(s). How many do you wish to sell?" << endl;
  int n;
  cin >> n;
  while(cin.fail())
    {
      resetBuffer();
      cin>>n;
    }
  while (n<1 || n>count)
    {
      cout << "Invalid number! Don't try to break the game, enter another number:" << endl;
      cin>> n;
    }
  return n;
}

int Human::askHowManyGoods(Game* game) {
    unsigned int numCards;
    cout << "How many cards you would like to take?" << endl;
    cin >> numCards;
    while(cin.fail())
      {
	resetBuffer();
	cin>>numCards;
      }
    while (numCards > hand.size() || numCards > (unsigned)game->countGoodsInMarket() || numCards <= 1) {
      if(numCards > hand.size())
	cout << "You don't have enough cards in your hand. ";
      if(numCards > (unsigned)game->countGoodsInMarket())
	cout << "There aren't enough goods in the market. ";
      if(numCards <= 1)
	cout << "You must exchange at least two goods. ";
      cout << "How many cards you would like to take?" << endl;
        cin >> numCards;
    }
    
    return numCards;
}
int Human::askWhichGood(Game* game) {
  unsigned int option;
  cout << "Which item would you like from the market? ";
  cin >> option;
  while(cin.fail())
    {
      resetBuffer();
      cin>>option;
    }
    while (option > game->market.size() || option <= 0 || !game->isGoodInMarket(option)) {
        cout << "That card isn't there.  Which item would you like from the market?" << endl;
        cin >> option;
    }
  return option - 1;
}

int Human::askGoodToMarket() {
    unsigned int option;
    dispHand();
    cout << "Which item would you like to put in market from your hand, enter 0 for camel? ";
    cin >> option;
    while(cin.fail())
      {
	resetBuffer();
	cin>>option;
      }
    while (option > hand.size() || (option == 0 && herd.size() == 0)) {
        cout << "That card isn't there.  Which item would you like to put in market from your hand?" << endl;
        cin >> option;
    }
    return option - 1;
}

int Human::askWhichGood_again() {
  int option;
  cout << "You may only take goods. Make another selection:" << endl;;
  cin >> option;
  while(cin.fail())
    {
      resetBuffer();
      cin>>option;
    }
  return option - 1;
}

void Human::resetBuffer(){
  cin.clear();
  cin.ignore(std::numeric_limits<std::streamsize>::max(),'\n');
  cout << "Please don't try to break the program. Enter a number: "<<endl;
}
